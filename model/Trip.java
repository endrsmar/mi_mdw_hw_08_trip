/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

/**
 * @author Martin Endrst
 */
public class Trip extends BaseEntity {
    
    public String destination;
    public int capacity;
    public double price;

    public Trip(){}
    public Trip(String destination, int capacity, double price){
        this.destination = destination;
        this.capacity = capacity;
        this.price = price;
    }
    public Trip(int id, String destination, int capacity, double price){
        this.id = id;
        this.destination = destination;
        this.capacity = capacity;
        this.price = price;
    }
    
}
