/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

/**
 * @author Martin Endrst
 */
public class TripBooking extends BaseEntity {
    
    public int tripId;
    public String name;
    public int persons;
    
    public TripBooking(){}
    public TripBooking(int tripId, String name, int persons){
        this.tripId = tripId;
        this.name = name;
        this.persons = persons;
    }
    
}
