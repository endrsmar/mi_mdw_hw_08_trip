/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

import java.io.Serializable;

/**
 * @author Martin Endrst
 */
public abstract class BaseEntity implements Serializable{
    
    public int id;
    
}
