/*
 * Created by MartinEndrst as part of MI-MDW course at CTU FIT
 */
package persistence;

import java.util.ArrayList;
import java.util.List;
import model.TripBooking;

/**
 * @author Martin Endrst
 */
public class TripBookingDB {
    private static final List<TripBooking> database = new ArrayList<TripBooking>();
    private static int lastId = 0;
    
    public static final void persist(TripBooking booking){
        booking.id = ++lastId;
        database.add(booking);
    }
    
    public static final TripBooking find(int id){
        for (TripBooking booking : database){
            if (booking.id == id){
                return booking;
            }
        }
        return null;
    }
    
    public static List<TripBooking> findAll(){
        return database;
    }
    
    public static void delete(int id){
        TripBooking booking = find(id);
        if (booking != null){
            database.remove(booking);
        }
    }
}
