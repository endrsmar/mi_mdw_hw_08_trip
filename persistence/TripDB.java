/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package persistence;

import java.util.ArrayList;
import java.util.List;
import model.Trip;

/**
 * @author Martin Endrst
 */
public class TripDB {
    private static final List<Trip> database = new ArrayList<Trip>();
    private static int lastId = 0;
    
    public static final void persist(Trip trip){
        trip.id = ++lastId;
        database.add(trip);
    }
    
    public static final Trip find(int id){
        for (Trip trip : database){
            if (trip.id == id) return trip;
        }
        return null;
    }
    
    public static final List<Trip> findAll(){
        return database;
    }
    
    public static final void delete(int id){
        Trip trip = find(id);
        if (trip != null){
            database.remove(trip);
        }
    }
    
    public static final List<Trip> findByDestination(String destination){
        List<Trip> retval = new ArrayList<Trip>();
        for (Trip trip : database){
            if (trip.destination.equals(destination)) retval.add(trip);
        }
        return retval;
    }
    
}
