/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trip_service;

import fault.CapacityInsufficient;
import fault.TripNotFound;
import java.util.List;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import model.Trip;
import model.TripBooking;
import persistence.TripBookingDB;
import persistence.TripDB;

/**
 *
 * @author vagrant
 */
@WebService(serviceName = "TripService")
public class TripService {
    
    @WebMethod(operationName = "listTrips")
    @WebResult(name="trip")
    public List<Trip> listTrips() {
        return TripDB.findAll();
    }
    
    @WebMethod(operationName = "createTrip")
    @WebResult(name="trip")
    public Trip createTrip(@WebParam(name="name") String name,
                          @WebParam(name="capacity") int capacity,
                          @WebParam(name="price") double price) {
        Trip trip = new Trip(name, capacity, price);
        TripDB.persist(trip);
        return trip;
    }
    
    @WebMethod(operationName = "listTripBookings")
    @WebResult(name="trip")
    public List<TripBooking> listTripBookings() {
        return TripBookingDB.findAll();
    }
    
    @WebMethod(operationName = "createTripBooking")
    @WebResult(name="booking")
    public TripBooking createTripBooking(@WebParam(name="tripId") int tripId,
                             @WebParam(name="name") String name,
                             @WebParam(name="persons") int persons)
    throws CapacityInsufficient, TripNotFound{
        Trip trip = TripDB.find(tripId);
        if (trip == null){
            throw new TripNotFound();
        } else if (trip.capacity < persons){
            throw new CapacityInsufficient();
        }
        trip.capacity -= persons;
        
        TripBooking booking = new TripBooking(tripId, name, persons);
        TripBookingDB.persist(booking);
        return booking;
    }
    
}
