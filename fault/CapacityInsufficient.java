/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package fault;

/**
 * @author Martin Endrst
 */
public class CapacityInsufficient extends Exception {
    
    public CapacityInsufficient(){
        super("Capacity insufficient");
    }
    
}
